#include "stdafx.h"
/*
* 20171229_mj: ok unit tests present, still will leave it here for quick checks
* 20171228_mj: dirty hack for DEV testing that is going to be replaced with unit test soon. be gone console!
*/
int main()
{

#pragma region naive
	std::vector<std::vector<float>> a;
	std::vector<std::vector<float>> b;

	a = { { 1,2 },{ 3,4 } };
	b = { { 0,1 },{ 0,0 } };

	auto start = std::chrono::high_resolution_clock::now();
	auto res = matrix::multply_mtrx(a, b);
	auto finish = std::chrono::high_resolution_clock::now();

	auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);
	std::cout << "execution(nanoseconds): " << nanoseconds.count() << "\n";

	for (size_t i = 0; i < a.size(); i++)
	{
		printf("| ");
		for (size_t z = 0; z < a[i].size(); z++)
		{
			printf("%F ", a[i][z]);
		}
		printf("|\n");
	}

	a = { { 1,2,3 } };
	b = { { 1,2,3 },{ 1,2,3 },{ 1,2,3 } };

	start = std::chrono::high_resolution_clock::now();
	matrix::multply_mtrx(a, b);
	finish = std::chrono::high_resolution_clock::now();

	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);
	std::cout << "execution(nanoseconds): " << nanoseconds.count() << "\n";

	if (res == 0) {
		for (size_t i = 0; i < a.size(); i++)
		{
			printf("| ");
			for (size_t z = 0; z < a[i].size(); z++)
			{
				printf("%F ", a[i][z]);
			}
			printf("|\n");
		}
	}
	else {
		printf("error");
	}
#pragma endregion
	std::cout << "\n";
#pragma region low
	float** c = new float*[2];
	c[0] = new float[2]{ 1,2 };
	c[1] = new float[2]{ 3,4 };

	float ** d = new float*[2];
	d[0] = new float[2]{ 0,1 };
	d[1] = new float[2]{ 0,0 };
	start = std::chrono::high_resolution_clock::now();
	matrix::multply_mtrx_low(c, d, 2, false);
	finish = std::chrono::high_resolution_clock::now();

	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);
	std::cout << "low lvl execution(nanoseconds): " << nanoseconds.count() << "\n";

	for (size_t i = 0; i < 2; i++)
	{
		printf("| ");
		for (size_t z = 0; z < 2; z++)
		{
			printf("%F ", c[i][z]);
		}
		printf("|\n");
	}

	c = new float*[1];
	c[0] = new float[3]{ 1,2,3 };

	d = new float*[3];
	d[0] = new float[3]{ 1,2,3 };
	d[1] = new float[3]{ 1,2,3 };
	d[2] = new float[3]{ 1,2,3 };

	start = std::chrono::high_resolution_clock::now();
	matrix::multply_mtrx_low(c, d, 3, true);
	finish = std::chrono::high_resolution_clock::now();

	nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(finish - start);
	std::cout << "low lvl execution(nanoseconds): " << nanoseconds.count() << "\n";

	for (size_t i = 0; i < 1; i++)
	{
		printf("| ");
		for (size_t z = 0; z < 3; z++)
		{
			printf("%F ", c[i][z]);
		}
		printf("|\n");
	}
#pragma endregion
	 
	char* tmp = new char[10];
	std::cin >> tmp;

    return 0;
}

