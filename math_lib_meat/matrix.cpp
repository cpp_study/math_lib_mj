/*
* 20180102_mj: implementation of matrix.h
* lots of code duplication as we err on the side of performance for the most part (exept naive one)
* rather KISS than SOLID
*/

#include "stdafx.h"
#include "matrix.h"

void matrix::multply_mtrx_low(float**& target, float**& mtrx, const size_t mtrx_size, bool vect) {

	auto sz = vect ? 1 : mtrx_size;

	//init the result array
	auto res = new float*[sz];

	for (size_t i = 0; i < sz; i++)
	{
		res[i] = new float[mtrx_size];
	}

	//calculate
	for (size_t i = 0; i < sz; i++)
	{
		for (size_t j = 0; j < mtrx_size; j++)
		{
			res[i][j] = float(0);

			for (size_t z = 0; z < mtrx_size; z++)
			{
				res[i][j] += target[i][z] * mtrx[z][j];
			}
		}
	}

	//assign result
	auto to_cleanup = target;
	target = res;

	//cleanup memory
	for (size_t i = 0; i < sz; i++)
	{
		delete to_cleanup[i];
	}
}

void matrix::add_mtrx(float**& target, float**& mtrx, const size_t mtrx_size) {

	//init the result array
	auto res = new float*[mtrx_size];

	for (size_t i = 0; i < mtrx_size; i++)
	{
		res[i] = new float[mtrx_size];
	}

	//calculate
	for (size_t i = 0; i < mtrx_size; i++)
	{
		for (size_t j = 0; j < mtrx_size; j++)
		{
			res[i][j] = target[i][j] + mtrx[i][j];
		}
	}

	//assign result
	auto to_cleanup = target;
	target = res;

	//cleanup memory
	for (size_t i = 0; i < mtrx_size; i++)
	{
		delete to_cleanup[i];
	}
}

void matrix::subtract_mtrx(float**& target, float**& mtrx, const size_t mtrx_size) {

	//init the result array
	auto res = new float*[mtrx_size];

	for (size_t i = 0; i < mtrx_size; i++)
	{
		res[i] = new float[mtrx_size];
	}

	//calculate
	for (size_t i = 0; i < mtrx_size; i++)
	{
		for (size_t j = 0; j < mtrx_size; j++)
		{
			res[i][j] = target[i][j] - mtrx[i][j];
		}
	}

	//assign result
	auto to_cleanup = target;
	target = res;

	//cleanup memory
	for (size_t i = 0; i < mtrx_size; i++)
	{
		delete to_cleanup[i];
	}
}

void matrix::transpose_mtrx(float**& target, const size_t mtrx_size) {

	//init the result array
	auto res = new float*[mtrx_size];

	for (size_t i = 0; i < mtrx_size; i++)
	{
		res[i] = new float[mtrx_size];
	}
	auto low_bound = mtrx_size - 1;
	//calculate
	for (size_t i = 0; i < mtrx_size; i++)
	{
		for (size_t j = (mtrx_size); j > 0; j--)
		{
			res[i][j-1] = target[j-1][i];
		}
	}

	//assign result
	auto to_cleanup = target;
	target = res;

	//cleanup memory
	for (size_t i = 0; i < mtrx_size; i++)
	{
		delete to_cleanup[i];
	}
}

//flip matrix to get columns 
static void set_columns(std::vector<std::vector<float>>& columns, const std::vector<std::vector<float>>& mtrx) {
	for (size_t i = 0; i < mtrx[0].size(); i++)
	{
		columns.push_back(std::vector<float>());
	}

	for (size_t i = 0; i < mtrx[0].size(); i++)
	{
		for (size_t z = 0; z < mtrx.size(); z++)
		{
			columns[i].push_back(mtrx[z][i]);
		}
	}
}

//ensure all conditions are matched
static bool check_matr(const std::vector<std::vector<float>>& target, const std::vector<std::vector<float>>& mtrx) {
	if (target.empty() || mtrx.empty() || (target.size() != 1 && target.size() != mtrx.size())) {
		return false;
	}

	auto col_size = size_t(0);
	auto res = true;

	for (size_t i = 0; i < mtrx.size(); i++)
	{
		if (i == 0) {
			col_size = target[i].size();
		}

		if ( i < target.size() && col_size != target[i].size() || col_size != mtrx[i].size()) {
			res = false;
			break;
		}
	}

	return res;
}

int matrix::multply_mtrx(std::vector<std::vector<float>>& target, const std::vector<std::vector<float>>& mtrx) {
	if (!check_matr(target, mtrx)) {
		return -1;
	}

	auto columns = std::vector<std::vector<float>>();

	set_columns(columns, mtrx);

	for (size_t i = 0; i < target.size(); i++)
	{
		auto res = std::vector<float>();

		for (size_t z = 0; z < target[i].size(); z++)
		{
			res.push_back(0);
			for (size_t x = 0; x < target[i].size(); x++)
			{
				res[z] += target[i][x] * columns[z][x];
			}			
		}

		for (size_t z = 0; z < res.size(); z++)
		{
			target[i][z] = res[z];
		}
	}
	
	return 0;
}

	
