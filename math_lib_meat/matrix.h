#pragma once

#include <vector>
/*
* 20180102_mj: this namespace provides matrix calculation, limited to square matrixes of the same size
* or vector (i.e. 1x3) and square matrix of same lengths(3x3) 
* as this is what's needed for game engine most of the time
*/
namespace matrix {
	//20180101_mj: low level implementation manipulating memory directly.
	// this called raw is circa 50x - 100x faster than the naive one
	void multply_mtrx_low(float**& target, float**& mtrx, const size_t mtrx_size, bool vect);

	// 20171229_mj: kind of naive implementation without directly fucking about with memory
	int multply_mtrx(std::vector<std::vector<float>>& target, const std::vector<std::vector<float>>& mtrx);	

	//20180102_mj: add square matrices. low level implementation manipulating memory directly.
	void add_mtrx(float**& target, float**& mtrx, const size_t mtrx_size);

	//20180102_mj: subtract square matrices. low level implementation manipulating memory directly.
	void subtract_mtrx(float**& target, float**& mtrx, const size_t mtrx_size);

	//20180102_mj: transpose square matrices. low level implementation manipulating memory directly.
	void transpose_mtrx(float**& target, const size_t mtrx_size);
}
