#include "stdafx.h"
#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace testmath_lib_meat
{		
	TEST_CLASS(matrix_test)
	{
	public:
		// naive implementation tested. happy and fail paths
		TEST_METHOD(matrix_multiply)
		{
			std::vector<std::vector<float>> a = { { 1,2 },{ 3,4 } };
			std::vector<std::vector<float>> b = { { 0,1 },{ 0,0 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(0, res);

			Assert::AreEqual(zero, a[0][0]);
			Assert::AreEqual(float(1), a[0][1]);

			Assert::AreEqual(zero, a[1][0]);
			Assert::AreEqual(float(3), a[1][1]);
		}

		TEST_METHOD(matrix_multiply_vector)
		{
			std::vector<std::vector<float>> a = { { 1,2,3 } };
			std::vector<std::vector<float>> b = { { 1,2,3 },{ 1,2,3 },{ 1,2,3 } };

			auto res = matrix::multply_mtrx(a, b);

			Assert::AreEqual(0, res);

			Assert::AreEqual(float(6), a[0][0]);
			Assert::AreEqual(float(12), a[0][1]);
			Assert::AreEqual(float(18), a[0][2]);
		}

		TEST_METHOD(matrix_multiply_fail_1)
		{
			std::vector<std::vector<float>> a = { { 1 },{ 3,4 } };
			std::vector<std::vector<float>> b = { { 0,1 },{ 0,0 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(-1, res);
		}

		TEST_METHOD(matrix_multiply_fail_2)
		{
			std::vector<std::vector<float>> a = { { 1,2 },{ 3,4 } };
			std::vector<std::vector<float>> b = { { 0,1,3 },{ 0,0 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(-1, res);
		}

		TEST_METHOD(matrix_multiply_fail_3)
		{
			std::vector<std::vector<float>> a = { { 1,2 },{ 3,4 }, { 3,4 } };
			std::vector<std::vector<float>> b = { { 0,1,3 },{ 0,0 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(-1, res);
		}

		TEST_METHOD(matrix_multiply_fail_4)
		{
			std::vector<std::vector<float>> a = { { 1,2 },{ 3,4 } };
			std::vector<std::vector<float>> b = { { 0,1,3 },{ 0,0 },{ 3,4 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(-1, res);
		}

		TEST_METHOD(matrix_multiply_fail_5)
		{
			std::vector<std::vector<float>> a;
			std::vector<std::vector<float>> b = { { 0,1,3 },{ 0,0 },{ 3,4 } };

			auto res = matrix::multply_mtrx(a, b);
			auto zero = float(0);

			Assert::AreEqual(-1, res);
		}

		// low lvl implementation. Only happy path tested as it seems a lot of paint to figure out size of an array
		// after it has been collapsed to a pointer when passing to function
		TEST_METHOD(matrix_multiply_low)
		{
			float** a = new float*[2];
			a[0] = new float[2]{ 1,2 };
			a[1] = new float[2]{ 3,4 };

			float ** b = new float*[2];
			b[0] = new float[2]{ 0,1 };
			b[1] = new float[2]{ 0,0 };

			matrix::multply_mtrx_low(a, b, 2, false);
			auto zero = float(0);

			Assert::AreEqual(zero, a[0][0]);
			Assert::AreEqual(float(1), a[0][1]);

			Assert::AreEqual(zero, a[1][0]);
			Assert::AreEqual(float(3), a[1][1]);
		}

		TEST_METHOD(matrix_multiply_vector_low)
		{
			float** a = new float*[1];
			a[0] = new float[3]{ 1,2,3 };

			float** b = new float*[3];
			b[0] = new float[3]{ 1,2,3 };
			b[1] = new float[3]{ 1,2,3 };
			b[2] = new float[3]{ 1,2,3 };


			matrix::multply_mtrx_low(a, b, 3, true);

			Assert::AreEqual(float(6), a[0][0]);
			Assert::AreEqual(float(12), a[0][1]);
			Assert::AreEqual(float(18), a[0][2]);
		}

		TEST_METHOD(matrix_add_low)
		{
			float** a = new float*[2];
			a[0] = new float[2]{ 1,2 };
			a[1] = new float[2]{ 3,4 };

			float ** b = new float*[2];
			b[0] = new float[2]{ 0,1 };
			b[1] = new float[2]{ 0,0 };

			matrix::add_mtrx(a, b, 2);

			auto zero = float(0);

			Assert::AreEqual(float(1), a[0][0]);
			Assert::AreEqual(float(3), a[0][1]);

			Assert::AreEqual(float(3), a[1][0]);
			Assert::AreEqual(float(4), a[1][1]);
		}

		TEST_METHOD(matrix_subtract_low)
		{
			float** a = new float*[2];
			a[0] = new float[2]{ 1,2 };
			a[1] = new float[2]{ 3,4 };

			float ** b = new float*[2];
			b[0] = new float[2]{ 2,1 };
			b[1] = new float[2]{ 2,4 };

			matrix::subtract_mtrx(a, b, 2);
			auto zero = float(0);

			Assert::AreEqual(float(-1), a[0][0]);
			Assert::AreEqual(float(1), a[0][1]);

			Assert::AreEqual(float(1), a[1][0]);
			Assert::AreEqual(float(0), a[1][1]);
		}

		TEST_METHOD(matrix_transpose_low_1)
		{
			float** a = new float*[3];
			a[0] = new float[3]{ 1,2,3 };
			a[1] = new float[3]{ 4,5,6 };
			a[2] = new float[3]{ 7,8,9 };


			matrix::transpose_mtrx(a, 3);

			Assert::AreEqual(float(1), a[0][0]);
			Assert::AreEqual(float(4), a[0][1]);
			Assert::AreEqual(float(7), a[0][2]);

			Assert::AreEqual(float(2), a[1][0]);
			Assert::AreEqual(float(5), a[1][1]);
			Assert::AreEqual(float(8), a[1][2]);

			Assert::AreEqual(float(3), a[2][0]);
			Assert::AreEqual(float(6), a[2][1]);
			Assert::AreEqual(float(9), a[2][2]);

		}

		TEST_METHOD(matrix_transpose_low_2)
		{
			float** a = new float*[4];
			a[0] = new float[4]{ 1, 2, 3, 4 };
			a[1] = new float[4]{ 5, 6, 7, 8 };
			a[2] = new float[4]{ 9, 10,11,12 };
			a[3] = new float[4]{ 13,14,15,16 };


			matrix::transpose_mtrx(a, 4);

			Assert::AreEqual(float(1), a[0][0]);
			Assert::AreEqual(float(5), a[0][1]);
			Assert::AreEqual(float(9), a[0][2]);
			Assert::AreEqual(float(13), a[0][3]);

			Assert::AreEqual(float(2), a[1][0]);
			Assert::AreEqual(float(6), a[1][1]);
			Assert::AreEqual(float(10), a[1][2]);
			Assert::AreEqual(float(14), a[1][3]);

			Assert::AreEqual(float(3), a[2][0]);
			Assert::AreEqual(float(7), a[2][1]);
			Assert::AreEqual(float(11), a[2][2]);
			Assert::AreEqual(float(15), a[2][3]);

			Assert::AreEqual(float(4), a[3][0]);
			Assert::AreEqual(float(8), a[3][1]);
			Assert::AreEqual(float(12), a[3][2]);
			Assert::AreEqual(float(16), a[3][3]);
		}
	};
}